## Flock Integration

Flock notification for ErpNext

#How to Install

**Install App**

1. bench get-app flock https://prashanta@bitbucket.org/prashanta/flock.git

2. bench install-app flock

#In Flock Menu

1. Go to https://docs.flock.com/display/flockos/Create+An+Incoming+Webhook

2. Create a "Incoming WebHooks"

3. Copy the generated Webhook URL value

* It should look like this: https://api.flock.com/hooks/sendMessage/a9809b15-d940-495a-bba3-9833e5c5fb91

#In Frappe/ERPNext Menu

1. Go to Setup -> Flockhook

2. Set the the values.

Mandatory Fields:

	* Request URL = Flock generated Incoming webhook URL

	* Doctype = ErpNext Doctype name

	* Event = Select an event (i.e after_insert,on_submit,on_cancel,on_trash)

Message Body Example

You can setup message body like Email Alert message section use doctype fields name like-

```
{% for i in doc.items %}
Item: {{i.item_name }} Qty: {{ i.qty }}
{% endfor %}

<br>Customer: {{ doc.customer }}
<br>Amount: {{ doc.total_amount }}
<br>Amount: {{ doc.company }}
```