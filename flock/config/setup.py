from __future__ import unicode_literals
from frappe import _


def get_data():
    return [
        {
            "label": _("Integrations"),
            "icon": "octicon octicon-comment",
            "items": [
                {
                    "type": "doctype",
                    "name": "Flockhook",
                    "label": _("Flockhook"),
                    "description": _("Flock notification for ERPNext"),
                    "hide_count": True
                }
            ]
        }
]