# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Flock",
			"color": "grey",
			"icon": "octicon octicon-comment",
			"type": "module",
			"hidden": 1,
			"label": _("Flock")
		}
	]
