import json
import frappe
from urlparse import urlparse
from frappe import _
def notify_flock(doc, method):
    send_flock(doc,doc.doctype,method, frappe.session.user)

def send_flock(doc,doctype,event, username):
    import httplib
    request_url = frappe.db.get_value('Flockhook', {"hook_doctype": doctype,"docevent": event,'enabled': 1}, 'webhook_url')
    if request_url:
        subject = get_subject(doc.doctype,event,frappe.session.user)
        message = get_message(doc,doc.doctype,event)
        color = get_color(event)
        parsed_uri = urlparse(request_url)
        domain = '{uri.netloc}'.format(uri=parsed_uri)
        path = '{uri.path}'.format(uri=parsed_uri)
        data = json.dumps({"from": username, "text": subject, "attachments": [{
        "title": doc.name,
        "color": color,
        "views": {
            "flockml": "<flockml>"+message+"</flockml>",
        }
        }] })
        h = httplib.HTTPSConnection(domain)
        headers = {"Content-type": "application/json", "Accept": "application/json"}
        h.request('POST', path, data, headers)
        r = h.getresponse()     
def get_subject(doctype,event, username):
    if event == 'after_insert':
        return "["+username+"]- Create a "+doctype
    elif event == 'on_submit':
        return "["+username+"]- Submit a "+doctype
    elif event == 'on_cancel':
        return "["+username+"]- Cancel a "+doctype
    elif event == 'on_trash':
        return "["+username+"]- Remove a "+doctype

def get_message(doc,doctype,event):
    message = frappe.db.get_value('Flockhook', {"hook_doctype": doctype,"docevent": event,'enabled': 1}, 'message')
    context = {"doc": doc, "alert": doc.name}
    msg = frappe.render_template(message, context)  
    return msg
def get_color(event):
    if event == 'after_insert':
        return "#33ff5b"
    elif event == 'on_submit':
        return "#3389ff"
    elif event == 'on_cancel':
        return "#eeff33"
    elif event == 'on_trash':
        return "#ff4833"