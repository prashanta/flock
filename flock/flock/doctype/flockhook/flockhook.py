# -*- coding: utf-8 -*-
# Copyright (c) 2018, pmahato@rawjute.in and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from urlparse import urlparse
from frappe.model.document import Document
from frappe import _

class Flockhook(Document):
	def autoname(self):
		self.name = self.hook_doctype + "-" + self.docevent
	
	def validate(self):
		self.validate_docevent()
		self.validate_request_url()


	def on_update(self):
		frappe.cache().delete_value('flock_webhook')


	def validate_docevent(self):
		if self.doctype:
			is_submittable = frappe.get_value("DocType", self.hook_doctype, "is_submittable")
			if not is_submittable and self.docevent in ["on_submit", "on_cancel"]:
				frappe.throw(_("DocType must be Submittable for the selected Doc Event"))

	def validate_request_url(self):
		try:
			request_url = urlparse(self.webhook_url).netloc
			if not request_url:
				raise frappe.ValidationError
		except Exception as e:
			frappe.throw(_("Check Request URL"), exc=e)	
